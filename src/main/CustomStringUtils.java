package main;

import java.util.ArrayDeque;
import java.util.Deque;

public class CustomStringUtils {

    public boolean areBracketsBalanced(String expression) {
        Deque<Character> openingBracketsStack = new ArrayDeque<>();

        for (char singleChar : expression.toCharArray()) {
            if (!isBracket(singleChar)) {
                continue;
            }
            if (isOpeningBracket(singleChar)) {
                openingBracketsStack.push(singleChar);
                continue;
            }
            if (openingBracketsStack.isEmpty()) {
                return false;
            }
            if (!isCorrectClosingBracket(openingBracketsStack.pop(), singleChar)) {
                return false;
            }
        }
        return openingBracketsStack.isEmpty();
    }


    private boolean isOpeningBracket(char input) {
        return input == '(' || input == '{' || input == '[';
    }

    private boolean isClosingBracket(char input) {
        return input == ')' || input == '}' || input == ']';
    }

    private boolean isBracket(char input) {
        return isOpeningBracket(input) || isClosingBracket(input);

    }

    private boolean isCorrectClosingBracket(char openingBracket, char closingBracket) {
        return switch (openingBracket) {
            case '(' -> closingBracket == ')';
            case '{' -> closingBracket == '}';
            case '[' -> closingBracket == ']';
            default -> false;
        };
    }
}
