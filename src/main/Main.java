package main;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Provide expression to be checked:");
        String expression = scanner.nextLine();

        CustomStringUtils customStringUtils = new CustomStringUtils();

        boolean isExpressionBalanced = customStringUtils.areBracketsBalanced(expression);

        if (isExpressionBalanced) {
            System.out.println("Expression is balanced");
        } else {
            System.out.println("Expression is not balanced");
        }

    }
}