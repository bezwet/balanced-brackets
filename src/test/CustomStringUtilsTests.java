package test;

import main.CustomStringUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomStringUtilsTests {

    @ParameterizedTest(name = "Expression checked: {0}. Expected result: {1}")
    @CsvSource({"(aa),true", "([),false", "(),true", "([]),true", "([{{}}]),true",
            "([{()}])[](){{}},true", "(A[1{ }4]22as), true", "abc), false"})
    public void getTextReversed_test(String expression, boolean result) {
        CustomStringUtils customStringUtils = new CustomStringUtils();

        boolean areBracketsBalanced = customStringUtils.areBracketsBalanced(expression);
        assertEquals(result, areBracketsBalanced);
    }
}
